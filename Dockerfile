FROM python:3.11
RUN useradd -r -m u2b
RUN apt update && apt install -y ffmpeg
RUN pip install --no-cache-dir python-ffmpeg yt-dlp
#COPY . /app
#WORKDIR /app
USER u2b
#ENV PORT=8080
#CMD ["python", "server.py"]